	#include <inttypes.h>
	#include <stdlib.h>
	#include "WProgram.h"

	#define _MMANAGER_STACK_MEMORY_MARGIN 128

	struct __memblock {
		size_t size;
		struct __memblock * next;
	};

	/* Heap Margins -> By Linker Script */

	extern unsigned long _ebss;
	extern unsigned long _estack;

	/* Basic Memory Manager Variables */

	volatile struct __memblock *  _membgn = (volatile struct __memblock *) ((char*) &_ebss + sizeof(struct __memblock));
	volatile struct __memblock *  _memend = (volatile struct __memblock *) ((char*) &_estack - _MMANAGER_STACK_MEMORY_MARGIN);
	volatile struct __memblock *  _membrr = (volatile struct __memblock *) ((char*) &_ebss + sizeof(struct __memblock));
	volatile struct __memblock *  _memfrn = (volatile struct __memblock *) &_ebss;

	/* Functions Definition */

	inline uint32_t mem_size();
	inline uint32_t mem_used();
	inline uint32_t mem_available();

	inline void * memrealloc(void*, size_t);
	inline void * memalloc(size_t);
	inline void   memfree(void*);

	/* Memmory Print (using Default Serial Port) -> REMOVE IT (only for Debug using Serial) */

	inline void	  memprint();

	/* Memmory Print (using Default Serial Port) -> REMOVE IT (only for Debug using Serial) */

	inline void	memprint()
	{

		struct __memblock *  _cfnode = (struct __memblock *) _memfrn->next;

		struct __memblock *  _cnode = (struct __memblock *) _membgn;
		Serial.println("{");
		do
		{
			Serial.print("{{");

			Serial.print((uint32_t) _cnode,HEX);

			Serial.print("|");

			Serial.print((uint32_t) _cnode + _cnode->size +sizeof(size_t)-1,HEX);
			
			Serial.print("}{");

			Serial.print(_cnode->size+sizeof(size_t));

			Serial.print("|");

			Serial.print(sizeof(size_t));

			Serial.print("|");

			Serial.print(_cnode->size);

			_cfnode = (struct __memblock *) _memfrn;

			if(_cfnode->next!= _cfnode && _cfnode->next)
				while ((_cfnode=_cfnode->next)!= (struct __memblock *)_memfrn) 
					if (_cfnode == _cnode) Serial.print("|F");

			Serial.println("}}");

		} while ((_cnode = (struct __memblock *)(((char*)_cnode) + sizeof(size_t) + _cnode->size)) != (struct __memblock*)_membrr);

		Serial.println("}");

		Serial.print("{");

		Serial.print(mem_size());

		Serial.print("|");

		Serial.print(mem_used());

		Serial.print("|");

		Serial.print(mem_available());

		Serial.println("}");

		Serial.println("}");

	}

	inline void * memalloc(size_t size)
	{
		/* If size is smaller than 8 bytes then align to 8 */

		size = size < 4 ? 4 : size;

		/* If First Free Block Pointer is not pointing to itself (free blocks available) */
		
		if (_memfrn != _memfrn->next && (_memfrn->next))
		{

			/* Current Free Node */

			struct __memblock *  _cfnode = (struct __memblock *) _memfrn->next;

			/* Previous Free Node */

			struct __memblock *  _pfnode = (struct __memblock *) _memfrn;

			/* Candidate */

			struct __memblock *  _cnnode=NULL;

			do
			{

				/* We found an equally sized free node (perfect case) */

				if (_cfnode->size == size)
				{

					/* Dispatching current node from the free list */

					_pfnode->next = _cfnode->next;
						
					/* Return the pointer */

					return((char*) (_cfnode->next));
				}

				/* We found a candidate however we are storing the previous node */

				if (_cfnode->size > size ) _cnnode = _pfnode;
						

				/* Updating previous node */

				_pfnode = _cfnode;

				/* Going to the next node and checking if it is the first one */

			} while ((_cfnode=_cfnode->next) != (struct __memblock *)_memfrn);

			/* In case a Candidate is available */

			if (_cnnode != NULL)
			{

				/* If it is too small to seperate return the hole node (limit of 8) */

				if ((_cnnode->next)->size - size < 8)
				{
					/* Point to the 'real free node' (not the previous one) */
					
					/* Dispatching current node from the free list */

					_cnnode->next = _cnnode->next->next;

					/* Return the pointer */

					return((char*) (_cnnode->next));
				}

				/* If we can seperate the node then {} */

				/* Point to the 'real free node' (not the previous one) */

				_cfnode = _cnnode->next;

				/* Point to the new free node */
					
				_pfnode = (struct __memblock *)((char*) _cfnode + size + sizeof(size_t));

				/* Set the size of the new free node */

				_pfnode->size = _cfnode->size - (size + sizeof(size_t));

				/* Set the size of our node */

				_cfnode->size = size;

				/* Exchange pointers (dispatching the old one and adding the new one) */

				_cnnode->next = _pfnode;

				_pfnode->next = _cfnode->next;

				/* Return the pointer */

				return((char*) (_cfnode->next));
			}
		}

		/* If the size of the available memory is less than the requested return NULL */

		if ((uint32_t) _membrr + sizeof(size_t) + size + 1 > (uint32_t) _memend)return NULL;

		/* Allocate a New Memory Block and Return a pointer of the data block */
		
		/* Assign the size of data */

		_membrr->size = size;

		/* Updates the position of the Memory Barrier */

		_membrr = (volatile struct __memblock *) ((char*) _membrr + size + sizeof(struct __memblock *));
			
		/* Returns the starting address for the data */

		return ((char*) _membrr - size);
	}

/*	inline void * memrealloc(void* pointer, size_t size)
	{

	}*/

	inline void memfree(void* pointer)
	{
		/* Typecasting the new free node */

		struct __memblock * _nfnode = (struct __memblock *) ((char*) pointer - sizeof(size_t));

		/* In case our Free List is Empty */

		if (_memfrn == _memfrn->next || !(_memfrn->next))
		{

			if (((char*) _nfnode) + _nfnode->size + sizeof(size_t)  == (char*) _membrr)
			{
				_membrr = _nfnode;
				return;
			}

			_memfrn->next = _nfnode;

			_nfnode->next = (struct __memblock *)_memfrn;		

			return;
		}

		/* In case our Free List is NOT Empty */

		struct __memblock *  _cfnode = (struct __memblock *) _memfrn->next;

		struct __memblock *  _pfnode = (struct __memblock *) _memfrn;

		do
		{
			if ((uint32_t) _cfnode > (uint32_t) _nfnode)
			{
				_pfnode->next = _nfnode;
				_nfnode->next = _cfnode;

				if (_nfnode == (struct __memblock *)(((char*) _cfnode) -(sizeof(size_t)+ _nfnode->size)))
				{
					_nfnode->next = _cfnode->next;
					_nfnode->size += _cfnode->size + sizeof(size_t);
				}

				if (_nfnode == (struct __memblock *)(((char*) _pfnode) + (sizeof(size_t) + _pfnode->size)))
				{
					_pfnode->next = _nfnode->next;
					_pfnode->size += _nfnode->size + sizeof(size_t);
					_nfnode = _pfnode;
				}

				if (((char*) _nfnode) + _nfnode->size + sizeof(size_t) == (char*) _membrr)	_membrr = _nfnode;

				return;
			}

			_pfnode = _cfnode;

		} while ((_cfnode = _cfnode->next) != (struct __memblock *)_memfrn);

		if (_pfnode == (struct __memblock *)(((char*) _nfnode) - (sizeof(size_t) + _pfnode->size)))
		{
			_pfnode->next = (struct __memblock *)_memfrn;
			_pfnode->size += _nfnode->size + sizeof(size_t);

			if (((char*) _pfnode) + _pfnode->size + sizeof(size_t) == (char*) _membrr)	_membrr = _pfnode;
		}
		else
		{
			if (((char*) _nfnode) + _nfnode->size + sizeof(size_t) == (char*) _membrr)
			{
				_membrr = _nfnode;

				return;
			}
			_nfnode->next = (struct __memblock *)_memfrn;
			_pfnode->next = _nfnode;
		}
		return;
	}

	inline uint32_t mem_size()
	{
		return((uint32_t) _memend - (uint32_t) _membgn);
	}

	inline uint32_t mem_used()
	{
		uint32_t result = (uint32_t) _membrr - (uint32_t) _membgn;

		if (_memfrn == _memfrn->next || !(_memfrn->next)) return (result);

		struct __memblock *  _cfnode = (struct __memblock *) _memfrn->next;

		do
		{

			result -= _cfnode->size;

		} while ((_cfnode = _cfnode->next) != (struct __memblock *)_memfrn);

		return (result);
	}

	inline uint32_t mem_available()
	{
		uint32_t result = (uint32_t) _memend - (uint32_t) _membrr;

		if (_memfrn == _memfrn->next ||  !(_memfrn->next)) return (result);

		struct __memblock *  _cfnode = (struct __memblock *) _memfrn->next;

		do
		{
			result += _cfnode->size;

		} while ((_cfnode = _cfnode->next) != (struct __memblock *)_memfrn);

		return (result);
	}
