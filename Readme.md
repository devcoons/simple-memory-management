###### Version: 0.1-alpha
Simple Memory Management Scheme
==================


  - [Synopsis](#synopsis)
  - [Compatibility](#compatibility)
  - [Requirements](#requirements)
  - [Installation](#installation)
  - [API](#api)
  - [Usage](#usage)
  - [Examples](#examples)
  - [Contributing](#contributing)
  - [License](#license)


## Synopsis

This Project is a memory allocation scheme that provides efficient dynamic memory allocation for small embedded systems lacking a Memory Management Unit (MMU). 
The proposed Simple Memory Management Scheme (SMM) maintains a balance between performance and efficiency, with the objective to increase the amount of usable memory in MMU-less embedded systems with a bounded and acceptable timing behavior. 

Its use requires no hardware MMU, and requires few or no manual changes to application software. 

## Compatibility
This is the main Compatibility section, listing all devices that are supported by Simple Memory Management Scheme.
<br/><br/>
  - PJRC Teensy Boards (3.0/3.1/3.2) [Using microVisual plugin for Microsoft Visual Studio and Arduino Software 1.6]
  - Arduino Mini (3.3v) [Using microVisual plugin for Microsoft Visual Studio and Arduino Software 1.4/1.5/1.6]
  - AVR 40 Pin 16MHz 32K 8A/D - ATMEGA32A-PU [Arduino Software 1.4/1.5/1.6 using ATMega support]
  - AVR 8 Pin 20MHz 8K 4A/D - ATtiny85 [Arduino Software 1.4/1.5/1.6 using ATtiny support] 
 
## Requirements

[ NONE ]

## Installation

Download the source code and copy both header and source files to your project's folder.
```
$ git clone https://gitlab.com/iikem/simple-memory-management.git  or  download as zip
$ cd "Source Code"/
```
This version can be used either on Arduino based platforms or Windows/Linux.

## API

Allocates a memory space and returns the address.
```
void * memalloc ( size_t );
```
Dispose a memory block.
```
void memfree ( void * );
```
Prints general memory information
```
void memprint ( );
```

## Usage

Make sure you have included the header file.
```
$ #include "mem_manager.h"
```
Step 1: declare a pointer
```
$ char * pointer;
```
Step 2: allocate some memory space and use the pointer as normal. (assign values etc)
```
$ pointer = (char *) memalloc ( X * sizeof(char) );
$ ...
$ ...
```
Step 4: free the allocated memory space
```
$ memfree(pointer);
```
## Examples
```
[ Under Construction ]
```

## Contributing
One of the easiest ways to contribute is to participate in discussions and discuss issues. You can also contribute by submitting pull requests with code changes. Please log a new issue in the appropriate section [Issues] or by e-mail.
The best way to get your bug fixed is to be as detailed as you can be about the problem.  Providing a minimal project with steps to reproduce the problem is ideal. 

Here are questions you can answer before you file a bug to make sure you're not missing any important information.

1. Did you read the documentation?
2. Did you include the command you executed in the issue?
3. What are the EXACT steps to reproduce this problem?
4. What package versions are you using?

## License

The MIT License (MIT)

Copyright (c) 2015-2016 Io. D

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.